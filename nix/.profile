export GOROOT=~/dev/go
export PATH=$PATH:~/bin:~/dev/mongodb/current/bin:$GOROOT/bin:
export GOPATH=$GOROOT/packages

export PS1='
[\u@\h `pwd`]
[`date "+%Y%m%d %H:%M:%S"`]\$: '


