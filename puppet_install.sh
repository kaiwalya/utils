dir=tmp$RANDOM
mkdir -p $dir
pushd $dir
curl -o facter.tar.gz https://downloads.puppetlabs.com/facter/facter-1.5.8.tar.gz ; tar -xvzf facter.tar.gz &
curl -o puppet.tar.gz https://downloads.puppetlabs.com/puppet/puppet-3.1.1.tar.gz ; tar -xvzf puppet.tar.gz &
wait
pushd facter-1.5.8
sudo ruby install.rb
popd
pushd puppet-3.1.1
sudo ruby install.rb
popd
popd
rm -rf $dir
