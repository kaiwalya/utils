sudo sysctl net.ipv4.conf.all.forwarding=1
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 4785
sudo iptables -t nat -A OUTPUT -p tcp --dport 80 -o lo -j REDIRECT --to-ports 4785
sudo iptables -t nat -A PREROUTING -p tcp --dport 443 -j REDIRECT --to-ports 4784
sudo iptables -t nat -A OUTPUT -p tcp --dport 443 -o lo -j REDIRECT --to-ports 4784
