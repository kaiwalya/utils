tmp=/tmp/gitutils/tmp$RANDOM
mkdir -p $tmp
curl -o $tmp/utils.tar.gz https://bitbucket.org/kaiwalya/utils/get/master.tar.gz
pushd $tmp && tar -xvf utils.tar.gz; popd
rm -rf $tmp/utils.tar.gz
repodir=$tmp/`find $tmp -type d | grep $tmp/kaiwalya-utils | sed s_"$tmp/"__ | grep -v /`
mkdir -p utils
mv $repodir/* ./utils
rm -rf /tmp/gitutils
