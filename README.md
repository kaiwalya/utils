Get Stuff
---------------

SSH Clone

	git clone git@bitbucket.org:kaiwalya/utils.git utils

HTTPS Clone

	git clone https://bitbucket.org/kaiwalya/utils.git utils

Download latest

	curl https://bitbucket.org/kaiwalya/utils/raw/master/getlatest.sh | bash
